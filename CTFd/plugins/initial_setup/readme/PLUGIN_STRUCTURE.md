# Plugin Structure

```text
CTFd
└── plugins
    └── initial_setup
        └── __init__.py
```

## `__init__.py`

This file is the CTFd plugin entrypoint that is called first when CTFd starts. In this case it contains all of the plugin code.
